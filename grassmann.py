import numpy as np
import random

def make_noise(c):
    vec = c.copy()
    t = random.expovariate(1)
    while t > 35 or t < 5: #######################
         t = random.expovariate(1)
    num = round(t)
    print("number of mistakes", num)
    index_of_vec_to_noise = random.sample(range(35), num)
    for index in index_of_vec_to_noise:
        vec[index] = (c[index] + 1)%2
    return vec
  
def increase(Vec):
    prev = 1
    for t in range(len(Vec)):
        if prev:
            if Vec[len(Vec) - t - 1] + 1 >= 2:
                Vec[len(Vec) - t - 1] = 0
            else:
                Vec[len(Vec) - t - 1] += 1
                prev = 0
    return Vec


def new_matrix(vec, i, j):
    A = [[0 for x in range(4)] for y in range(2)]
    A[0][i] = 1
    A[1][j] = 1
    k = 0
    for a in range(i+1, 4):
        if not a == j:
            A[0][a] = vec[k]
            k += 1
    for b in range(j+1, 4):
        A[1][b] = vec[k]
        k += 1
    return A

def minor(A):
    C = np.array([0 for x in range(6)])
    index = 0
    for a in range(4):
        for b in range(a+1, 4):
            C[index] = (A[0][a]*A[1][b] - A[0][b]*A[1][a])%2
            index += 1
    return C
            


def find_subspaces(multi_index):
    length = 8 - 3 - multi_index[0]-multi_index[1]
    vec = np.zeros(length)
    for index in range(2**length):
        Array_of_gr_points_row.append(minor(new_matrix(vec, multi_index[0], multi_index[1])))
        vec = increase(vec)



def change_col(i):
    for a in range(i, 35):
         if trans_array_gr_points[i][a] == 1:
            for x in range(6):
                s = trans_array_gr_points[x][i]
                trans_array_gr_points[x][i] = trans_array_gr_points[x][a]
                trans_array_gr_points[x][a] = s
            return None

def gauss(A):
    #A.sort(reverse=True)
    for i in range(6):
        if not trans_array_gr_points[i][i] == 1:
            change_col(i)
        for t in range(6):
            if trans_array_gr_points[t][i] != 0 and t != i:
                trans_array_gr_points[t] = (trans_array_gr_points[t] + trans_array_gr_points[i])%2
    return A



def find_smallest_mistake(sindr):
    vec = [0, 0, 0, 0, 0, 0]
    prev_weight = 35
    final_mistake = None
    counter_of_units = 0
    for t in range(64):
        mistake = np.array(vec + [0 for x in range(29)])
        for i in range(0, 29):
            mistake[i+6] = (sum(H_matrix[i, 0:6]*np.array(vec)) + sindr[i])%2
            counter_of_units += mistake[i+6]
            if counter_of_units > min([prev_weight, 6]):
                break
        c = sum(mistake)
        if  c < prev_weight and c:
            prev_weight = c
            final_mistake = mistake
        vec = increase(vec)
        counter_of_units = sum(np.array(vec))
    print("weight of the smallest", prev_weight)
    return final_mistake


n = 4
k = 2
Array_of_gr_points_row = []
for i in range(4):
    for j in range(i+1, 4):
        multi_index = np.array([i, j])
        find_subspaces(multi_index)

Array_of_gr_points = np.array(Array_of_gr_points_row)
trans_array_gr_points = Array_of_gr_points.transpose()
#Mas это набор точек (35 штук) грассманиана
G_matrix = gauss(trans_array_gr_points) #приводим к гаусс виду, чтобы сделать код систематическим
left_part_of_H = G_matrix[:, 6:36].transpose()

c = G_matrix[0] #элемент из кода C
right_part_of_H = np.array([[0 for x in range(29)] for y in range(29)])
for i in range(29):
    right_part_of_H[i][i] = 1
H_matrix = np.hstack((left_part_of_H, right_part_of_H)) #проверочная матрица
noisy_vec = make_noise(c)
sindr = np.dot(H_matrix, noisy_vec)%2 # синдром зашумленного вектора
if sum(sindr) == 0:
    print("0 mistakes")
    final = noisy_vec
else:
    smallest_mistake = find_smallest_mistake(sindr)
    if smallest_mistake != None:
        final = (noisy_vec + smallest_mistake)%2
    else:
        print("more than 6 mistakes")
print(sum(np.dot(H_matrix, final)%2))
print(c == final)





